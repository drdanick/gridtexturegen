# GridTextureGen #

This program will generate a grid of shaded and noisy squares for use as a texture.


## Requirements ##

This project requires OpenCV 2.x


## Compiling ##

The project is included with a simple build script for Linux, but will compile and work fine on Windows or OS X.


## Example outputs ##

**Unfiltered texture**

![Unfiltered texture](https://bitbucket.org/repo/bbpjMr/images/3336610175-texture.png)

**Filtered texture**

![Filtered texture](https://bitbucket.org/repo/bbpjMr/images/3352431891-filtered_texture.png)


## License ##

This program is provided free of license.